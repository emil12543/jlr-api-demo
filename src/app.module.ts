import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Product } from './products/product.entity';
import { ProductModule } from './products/product.module';
import { UserModule } from './users/user.module';
import { User } from './users/user.entity';
import { AuthModule } from './auth/auth.module';
import { Order } from './orders/order.entity';
import { OrderModule } from './orders/order.module';
import { OrderProduct } from './orders/orderProduct.entity';
import { LockerCell } from './lockers/lockerCell.entity';
import { Locker } from './lockers/locker.entity';
import { LockerModule } from './lockers/locker.module';
import { DailyMenuProduct } from './dailyMenuProducts/dailyMenuProduct.entity';
import { DailyMenuProductModule } from './dailyMenuProducts/dailyMenuProduct.module';
import { Voucher } from './vouchers/voucher.entity';
import { VoucherModule } from './vouchers/voucher.module';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'emilkostadinov',
      database: 'jlr',
      password: '',
      entities: [User, Product, Order, OrderProduct, Locker, LockerCell, DailyMenuProduct, Voucher],
      synchronize: true
    }),
    ProductModule,
    UserModule,
    AuthModule,
    OrderModule,
    LockerModule,
    DailyMenuProductModule,
    VoucherModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
