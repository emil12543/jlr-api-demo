import { Body, Controller, Post, Request, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from '../users/user.entity';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService
    ) {}

    @Post('/login')
    async login(@Body() user: User) {
        const validatedUser = await this.authService.validateUser(user.email, user.password);
        if (!validatedUser)
            throw new UnauthorizedException('Wrong credentials');

        return this.authService.login(user.email, validatedUser.role);
    }
}
