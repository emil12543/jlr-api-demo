import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User, UserRoleType } from '../users/user.entity';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../users/user.service';

@Injectable()
export class AuthService {
    constructor(
        private userService: UserService,
        private jwtService: JwtService
    ) {}

    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.userService.findOne(email);
        if (user && user.password === pass) {
            const { password, ...result } = user;
            return user;
        }
    }

    async login(email: string, role: UserRoleType) {
        const payload = { email, role };

        return {
            access_token: this.jwtService.sign(payload),
            role
        };
    }
}
