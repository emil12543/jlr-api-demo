import { Body, Controller, Get, Param, Post, Request } from '@nestjs/common';
import { DailyMenuProductService } from './dailyMenuProduct.service';
import { DailyMenuProduct } from './dailyMenuProduct.entity';

@Controller('dailymenu')
export class DailyMenuProductController {
    constructor(
        private dailyMenuProductService: DailyMenuProductService
    ) {}

    @Get(':date')
    public async getForDate(@Param('date') date: Date): Promise<DailyMenuProduct[]> {
        return await this.dailyMenuProductService.findForDate(date);
    }

    @Post(':date')
    public async createForDate(@Param('date') date: Date, @Body() products: any[]): Promise<DailyMenuProduct[]> {
        return await this.dailyMenuProductService.createForDate(date, products);
    }
}
