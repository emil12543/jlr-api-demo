import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from '../orders/order.entity';
import { Product } from '../products/product.entity';

@Entity('daily_menu_products')
export class DailyMenuProduct {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ nullable: true })
    description?: string;

    @Column()
    price: number;

    @Column('text', { array: true, nullable: true })
    ingredients?: string[];

    @ManyToOne(type => Product)
    originalProduct: Product;

    @Column()
    date: Date;

    @Column({ nullable: true })
    image: string;

    fromProduct(product: Product) {
        this.name = product.name;
        this.description = product.description;
        this.price = product.price;
        this.image = product.image;
        this.originalProduct = product;
        this.ingredients = product.ingredients;
    }
}
