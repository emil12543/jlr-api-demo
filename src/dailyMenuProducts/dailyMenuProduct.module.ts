import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DailyMenuProduct } from './dailyMenuProduct.entity';
import { DailyMenuProductService } from './dailyMenuProduct.service';
import { DailyMenuProductController } from './dailyMenuProduct.controller';
import { ProductModule } from '../products/product.module';

@Module({
    imports: [TypeOrmModule.forFeature([DailyMenuProduct]), ProductModule],
    providers: [DailyMenuProductService],
    exports: [DailyMenuProductService],
    controllers: [DailyMenuProductController]
})
export class DailyMenuProductModule {}
