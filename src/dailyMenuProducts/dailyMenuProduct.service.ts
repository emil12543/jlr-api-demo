import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DailyMenuProduct } from './dailyMenuProduct.entity';
import { Repository } from 'typeorm';
import { ProductService } from '../products/product.service';

@Injectable()
export class DailyMenuProductService {
    constructor(
        @InjectRepository(DailyMenuProduct)
        private dailyMenuProductRepository: Repository<DailyMenuProduct>,
        private productService: ProductService
    ) {}

    public async findForDate(date: Date): Promise<DailyMenuProduct[]> {
        return await this.dailyMenuProductRepository.find({
            where: {
                date
            }
        });
    }

    public async createForDate(date: Date, products: DailyMenuProduct[]): Promise<DailyMenuProduct[]> {
        const rdyProducts = [];
        await Promise.all(products.map(async (product) => {
            let dailyMenuProduct = new DailyMenuProduct();
            dailyMenuProduct.fromProduct(await this.productService.findOne(product.id));
            dailyMenuProduct.price = product.price;
            dailyMenuProduct.date = date;
            rdyProducts.push(await this.dailyMenuProductRepository.save(dailyMenuProduct));
        }));

        return rdyProducts;
    }

    public async findOne(id: number): Promise<DailyMenuProduct> {
        return await this.dailyMenuProductRepository.findOne(id);
    }
}
