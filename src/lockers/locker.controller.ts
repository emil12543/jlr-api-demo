import { Controller, Get } from '@nestjs/common';
import { LockerService } from './locker.service';
import { Locker } from './locker.entity';

@Controller('lockers')
export class LockerController {
    constructor(
        private lockerService: LockerService
    ) {}

    @Get()
    public async getAll(): Promise<any> {
        const lockers = await this.lockerService.getAll();
        return await Promise.all(lockers.map(async (locker) => {
            let isAvailable = false;
            try {
                isAvailable = !!await this.lockerService.findEmptyCellInLocker(locker.id);
            } catch (e) {

            }
            return {
                ...locker,
                isAvailable
            };
        }));
    }
}