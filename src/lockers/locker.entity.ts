import { Entity, OneToMany, PrimaryGeneratedColumn,  } from 'typeorm';
import { LockerCell } from './lockerCell.entity';

@Entity('lockers')
export class Locker {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToMany(type => LockerCell, cell => cell.locker)
    cells: LockerCell[];
}
