import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LockerCell } from './lockerCell.entity';
import { Locker } from './locker.entity';
import { LockerService } from './locker.service';
import { LockerController } from './locker.controller';

@Module({
    imports: [TypeOrmModule.forFeature([Locker, LockerCell])],
    providers: [LockerService],
    exports: [LockerService],
    controllers: [LockerController]
})
export class LockerModule {}
