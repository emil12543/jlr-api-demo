import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Locker } from './locker.entity';
import { IsNull, Repository } from 'typeorm';
import { LockerCell } from './lockerCell.entity';

@Injectable()
export class LockerService {
    constructor(
        @InjectRepository(Locker)
        private lockerRepository: Repository<Locker>,
        @InjectRepository(LockerCell)
        private lockerCellRepository: Repository<LockerCell>
    ) {}

    async findEmptyCellInLocker(lockerId: number): Promise<LockerCell> {
        const emptyCell = await this.lockerCellRepository.findOne({
            where: {
                currentOrderId: null,
                locker: {
                    id: lockerId
                }
            }
        });

        if (!emptyCell)
            throw new UnprocessableEntityException('There is no empty cell in this locker');

        return emptyCell;
    }

    async setCurrentOrderOnCell(orderId: number, cell: LockerCell): Promise<LockerCell> {
        cell.currentOrderId = orderId;
        return await this.lockerCellRepository.save(cell)
    }

    async getAll(): Promise<Locker[]> {
        return await this.lockerRepository.find({
            relations: ['cells']
        });
    }
}
