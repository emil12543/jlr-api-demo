import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Locker } from './locker.entity';
import { Order } from '../orders/order.entity';

@Entity('lockers_cells')
export class LockerCell {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Locker, locker => locker.cells, {
        eager: true
    })
    locker: Locker;

    @Column({ nullable: true })
    currentOrderId: number;
}
