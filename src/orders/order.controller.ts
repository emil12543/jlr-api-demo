import { Body, Controller, Post, UseGuards, Request, Put, Param, Get } from '@nestjs/common';
import { OrderService } from './order.service';
import { Order } from './order.entity';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UserService } from '../users/user.service';
import { Voucher } from '../vouchers/voucher.entity';
import { VoucherService } from '../vouchers/voucher.service';

interface CreateOrderDTO extends Order {
    vouchers: Voucher[];
}

@Controller('orders')
export class OrderController {
    constructor(
        private orderService: OrderService,
        private userService: UserService,
        private voucherService: VoucherService
    ) {}

    @UseGuards(JwtAuthGuard)
    @Get()
    async getAll(@Request() req): Promise<Order[]> {
        if (req.user.role === 'worker') {
            return await this.orderService.getAllByUser(req.user.email);
        } else {
            return await this.orderService.getAll();
        }
    }

    @UseGuards(JwtAuthGuard)
    @Post()
    async create(@Request() req, @Body() order: CreateOrderDTO): Promise<Order> {
        order.user = await this.userService.findOne(req.user.email);
        if (order.vouchers) {
            await Promise.all(order.vouchers.map(async (v: Voucher) => {
                const voucher = await this.voucherService.get(v.id);
                if (v.remainingAmount === 0)
                    return await this.voucherService.remove(voucher);
                else
                    return await this.voucherService.setRemaining(voucher, v.remainingAmount);
            }));
            delete order.vouchers;
        }

        return await this.orderService.create(order);
    }

    // @UseGuards(JwtAuthGuard)
    @Put(':id')
    async updateStatus(@Param() id: number, @Body() order: Order): Promise<Order> {
        return await this.orderService.updateStatus(order.status, id);
    }

    @Get(':id')
    async getOne(@Param() id: number): Promise<Order> {
        return await this.orderService.get(id);
    }
}
