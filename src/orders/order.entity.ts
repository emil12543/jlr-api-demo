import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn
} from 'typeorm';
import { User } from '../users/user.entity';
import { OrderProduct } from './orderProduct.entity';
import { LockerCell } from '../lockers/lockerCell.entity';
import { Product } from '../products/product.entity';

export type OrderStatus = 'preparing' | 'delivered' | 'picked';

@Entity('orders')
export class Order {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'enum',
        enum: ['preparing', 'delivered', 'picked'],
        default: 'preparing'
    })
    status: OrderStatus;

    @ManyToOne(type => User, user => user.orders, {
        eager: true
    })
    user: User;

    @OneToMany(type => OrderProduct, product => product.order, {
        cascade: ['insert'],
        eager: true
    })
    products: OrderProduct[];

    @ManyToOne(type => LockerCell, {
        eager: true
    })
    @JoinColumn()
    lockerCell: LockerCell;

    @CreateDateColumn()
    createdAt: string;
}
