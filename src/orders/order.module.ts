import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './order.entity';
import { OrderProduct } from './orderProduct.entity';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { UserModule } from '../users/user.module';
import { LockerModule } from '../lockers/locker.module';
import { ProductModule } from '../products/product.module';
import { DailyMenuProductModule } from '../dailyMenuProducts/dailyMenuProduct.module';
import { VoucherModule } from '../vouchers/voucher.module';

@Module({
    imports: [TypeOrmModule.forFeature([Order, OrderProduct]), UserModule, LockerModule, ProductModule, DailyMenuProductModule, VoucherModule],
    providers: [OrderService],
    controllers: [OrderController]
})
export class OrderModule {}
