import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Order, OrderStatus } from './order.entity';
import { Repository } from 'typeorm';
import { LockerService } from '../lockers/locker.service';
import { OrderProduct } from './orderProduct.entity';
import { ProductService } from '../products/product.service';
import { DailyMenuProduct } from '../dailyMenuProducts/dailyMenuProduct.entity';
import { DailyMenuProductService } from '../dailyMenuProducts/dailyMenuProduct.service';

@Injectable()
export class OrderService {
    constructor(
        @InjectRepository(Order)
        private orderRepository: Repository<Order>,
        @InjectRepository(OrderProduct)
        private orderProductRepository: Repository<OrderProduct>,
        private lockerService: LockerService,
        private dailyMenuProductService: DailyMenuProductService
    ) {}

    async get(id: number): Promise<Order> {
        return await this.orderRepository.findOne(id);
    }

    async getAll(): Promise<Order[]> {
        return await this.orderRepository.find();
    }

    async getAllByUser(email: string): Promise<Order[]> {
        return await this.orderRepository.find({
            where: {
                userEmail: email
            }
        });
    }

    async create(order: Order): Promise<Order> {
        order.lockerCell = await this.lockerService.findEmptyCellInLocker(order.lockerCell.locker.id);

        let products: OrderProduct[] = [];
        await Promise.all(order.products.map(async (product) => {
            let orderProduct = new OrderProduct();
            orderProduct.fromProduct(await this.dailyMenuProductService.findOne(product.id));
            orderProduct.ingredients = product.ingredients;
            orderProduct.excludedIngredients = product.excludedIngredients;

            products.push(orderProduct);
        }));
        order.products = products;

        order = await this.orderRepository.save(order);
        await this.lockerService.setCurrentOrderOnCell(order.id, order.lockerCell);
        return order;
    }

    async updateStatus(status: OrderStatus, orderId: number): Promise<Order> {
        const order = await this.orderRepository.findOne(orderId);

        if (!order)
            throw new BadRequestException('There is no order with this id');

        order.status = status;
        return await this.orderRepository.save(order);
    }
}
