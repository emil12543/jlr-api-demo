import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from './order.entity';
import { Product } from '../products/product.entity';
import { DailyMenuProduct } from '../dailyMenuProducts/dailyMenuProduct.entity';

@Entity('orders_products')
export class OrderProduct {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ nullable: true })
    description?: string;

    @Column()
    price: number; // in cents - 1000 -> 10.00 eur

    @Column('text', { array: true, nullable: true })
    ingredients?: string[];

    @Column('text', { array: true, nullable: true })
    excludedIngredients?: string[];

    @ManyToOne(type => DailyMenuProduct)
    dailyMenuProduct: Product;

    @ManyToOne(type => Order, order => order.products)
    order: Order;

    @Column({ nullable: true })
    image: string;

    fromProduct(product: DailyMenuProduct) {
        this.name = product.name;
        this.description = product.description;
        this.price = product.price;
        this.image = product.image;
        this.dailyMenuProduct = product;
    }
}
