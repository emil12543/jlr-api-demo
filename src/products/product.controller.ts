import { ProductService } from './product.service';
import { Body, Controller, Delete, Get, Param, Post, Put, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { Product } from './product.entity';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('products')
export class ProductController {
    constructor(
        private productService: ProductService
    ) {}

    @Get()
    public async getAll(): Promise<Product[]> {
        return await this.productService.findAll();
    }

    @Get('/ingredients')
    public async getAllIngredients(): Promise<string[]> {

        return await this.productService.getAllIngredients();
    }

    @Get(':id')
    public async getOne(@Param('id') id: number): Promise<Product> {
        return await this.productService.findOne(id);
    }

    @Post()
    public async create(@Body() product: Product): Promise<Product> {
        return await this.productService.create(product);
    }

    @Post('image')
    @UseInterceptors(
        FileInterceptor('image'),
    )
    public async uploadImage(@UploadedFile() file) {
        // const product = await this.productService.findOne(id);
        // product.image = file.filename;
        // await this.productService.update(id, product);
        console.log(file)
        const response = { filename: file.filename };
        return response;
    }

    @Get('image/:path')
    public async seeUploadedFile(@Param('path') path: string, @Res() res) {
        return res.sendFile(path, { root: './files' });
    }

    @Put(':id')
    public async update(@Param('id') id: number, @Body() product: Product): Promise<Product> {
        return await this.productService.update(id, product);
    }

    @Delete(':id')
    public async delete(@Param('id') id: number): Promise<void> {
        return await this.productService.remove(id);
    }
}
