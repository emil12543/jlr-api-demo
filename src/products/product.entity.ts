import { Column, Entity, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { OrderProduct } from '../orders/orderProduct.entity';

@Entity('products')
export class Product {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    name: string;

    @Column({ nullable: true })
    description?: string;

    @Column()
    price: number; // in cents - 1000 -> 10.00 eur, thats the current price

    @Column({ nullable: true })
    image: string;

    @Column('text', { array: true, nullable: true })
    ingredients?: string[];
}
