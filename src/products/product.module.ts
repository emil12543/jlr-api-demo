import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductService } from './product.service';
import { Product } from './product.entity';
import { ProductController } from './product.controller';
import { MulterModule } from '@nestjs/platform-express';

@Module({
    imports: [TypeOrmModule.forFeature([Product]), MulterModule.register({
        dest: './files',
        preservePath: true
    })],
    providers: [ProductService],
    exports: [ProductService],
    controllers: [ProductController],
})
export class ProductModule {}
