import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './product.entity';
import { getConnection, Repository } from 'typeorm';

@Injectable()
export class ProductService {
    constructor(
        @InjectRepository(Product)
        private productRepository: Repository<Product>
    ) {}

    async create(product: Product): Promise<Product> {
        if (await this.findOneByName(product.name))
            throw new BadRequestException('Product with this name already exists');

        return await this.productRepository.save(product);
    }

    async update(id: number, product: Product): Promise<Product> {
        if (!await this.findOne(id))
            throw new BadRequestException(`There is no product with ${ id } id`);

        product.id = id;
        await this.productRepository.save(product);
        return await this.findOne(id);
    }

    async findAll(): Promise<Product[]> {
        return await this.productRepository.find();
    }

    async findOne(id: number): Promise<Product> {
        const product = await this.productRepository.findOne(id);
        if (!product)
            throw new BadRequestException(`There is no product with ${ id } id`);

        return product;
    }

    async findOneByName(name: string): Promise<Product> {
        return await this.productRepository.findOne({
            where: {
                name
            }
        });
    }

    async remove(id: number): Promise<void> {
        if (!await this.findOne(id))
            throw new BadRequestException(`There is no product with ${ id } id`);

        await this.productRepository.delete(id);
    }

    async getAllIngredients(): Promise<string[]> {
        const ingredients = await this.productRepository
            .createQueryBuilder('products')
            .select('unnest(products.ingredients) as ingredient')
            .groupBy('ingredient')
            .getRawMany();

        return [...ingredients.map(({ ingredient }) => ingredient)];
    }
}
