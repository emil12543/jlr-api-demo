import {
    Body,
    Controller,
    Get,
    Post,
    Put,
    UseGuards,
    Request,
    UnauthorizedException,
    Param,
    Delete, Query
} from '@nestjs/common';
import * as nodemailer from 'nodemailer'
import { UserService } from './user.service';
import { User } from './user.entity';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

const transport = nodemailer.createTransport({
    // @ts-ignore
    host: 'mail.parcelhive.net',
    port: 465,
    secure: true,
    auth: {
        user: 'no-reply@parcelhive.net',
        pass: 'Noreply123!'
    },
    debug:true,
    tls: {rejectUnauthorized: false},
});

@Controller('/users')
export class UserController {
    constructor(
        private userService: UserService
    ) {}

    // @UseGuards(JwtAuthGuard)
    @Post()
    async register(@Body() user: User) {
        const newUser = await this.userService.create(user);

        const mail = {
            from: 'Demo ParcelHive <no-reply@parcelhive.net>',
            to: `${newUser.firstName} ${newUser.lastName} <${newUser.email}>`,
            subject: 'Demo ParcelHive Login Credentials',
            text: `Username/Email: ${ newUser.email }; Password: ${ newUser.password }`,
            html: `<p>Username/Email: ${ newUser.email }; Password: ${ newUser.password }</p>`,
        };

        const {err, info} = await transport.sendMail(mail);
        console.log(err, info);

        return newUser;
    }

    @UseGuards(JwtAuthGuard)
    @Put(':email')
    async update(@Param('email') email: string, @Body() user: User) {
        return await this.userService.update(email, user);
    }

    @UseGuards(JwtAuthGuard)
    @Delete(':email')
    async delete(@Param('email') email: string) {
        return await this.userService.remove(email);
    }

    // @UseGuards(JwtAuthGuard)
    @Get()
    async getAll(@Query('name') name) {
        return await this.userService.getAll(name);
    }
}
