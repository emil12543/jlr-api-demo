import { Column, Entity, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from '../orders/order.entity';
import { Voucher } from '../vouchers/voucher.entity';

export type UserRoleType = "cook" | "worker";

@Entity('users')
export class User {
    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @PrimaryColumn()
    email: string;

    @Column()
    password: string;

    @Column({
        type: "enum",
        enum: ["cook", "worker"],
        default: "worker"
    })
    role: UserRoleType;

    @OneToMany(type => Order, order => order.user)
    orders: Order[];

    @OneToMany(type => Voucher, voucher => voucher.user)
    vouchers: Voucher[];
}
