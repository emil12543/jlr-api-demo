import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Like, Repository } from 'typeorm';
import { BadRequestException, Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>
    ) {}

    async create(user: User): Promise<User> {
        if (await this.findOne(user.email))
            throw new BadRequestException('There is already a user with this email address');

        return await this.usersRepository.save(user);
    }

    async update(email: string, user: User): Promise<User> {
        if (!await this.findOne(email))
            throw new BadRequestException(`There is no user with this email address`);
        if (await this.findOne(user.email))
            throw new BadRequestException('There is already a user with this email address');

        user.email = email;
        await this.usersRepository.save(user);
        return await this.findOne(user.email);
    }

    async findOne(email: string): Promise<User> {
        return this.usersRepository.findOne(email);
    }

    async getAll(name?: string): Promise<User[]> {
        if (name)
            return await this.usersRepository.createQueryBuilder('users')
                .where(`LOWER( users.firstName ) LIKE LOWER( '%${name}%' )`)
                .orWhere(`LOWER( users.lastName ) LIKE LOWER( '%${name}%' )`)
                .andWhere(`users.role = 'worker'`)
                .getMany();

        return this.usersRepository.find({
            where: {
                role: 'worker'
            }
        });
    }

    async remove(email: string): Promise<void> {
        const user = await this.usersRepository.findOne(email);
        await this.usersRepository.delete(user);
        return;
    }
}
