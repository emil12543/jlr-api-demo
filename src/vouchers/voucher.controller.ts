import { Body, Controller, Get, Param, Post, Put, Request, UseGuards } from '@nestjs/common';
import { VoucherService } from './voucher.service';
import { UserService } from '../users/user.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('vouchers')
export class VoucherController {
    constructor(
        private voucherService: VoucherService,
        private userService: UserService
    ) {}

    @UseGuards(JwtAuthGuard)
    @Get()
    async getAll(@Request() req) {
        if (req.user.role === 'worker') {
            return await this.voucherService.getByUser(req.user.email);
        }
        return await this.voucherService.getAll();
    }

    @Get(':email')
    async getByUser(@Param('email') email: string) {
        return await this.voucherService.getByUser(email);
    }

    @Post()
    async create(@Body() body: { email: string; amount: number; details?: string; }) {
        return await this.voucherService.create(body.email, body.amount, body.details);
    }

    // @Put(':id')
    // async update(@Param('id') id: number, @Body() body: {  }) {
    //     const voucher = await this.voucherService.get(id);
    //     if (body.remainingAmount)
    //     // return await this.voucherService.update(body.email, body.amount);
    // }
}