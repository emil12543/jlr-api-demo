import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../users/user.entity';

@Entity('vouchers')
export class Voucher {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    amount: number;

    @Column()
    remainingAmount: number;

    @Column({ nullable: true })
    details?: string;

    @ManyToOne(type => User, user => user.vouchers, {
        eager: true
    })
    user: User;
}
