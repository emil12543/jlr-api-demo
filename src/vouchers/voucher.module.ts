import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Voucher } from './voucher.entity';
import { VoucherService } from './voucher.service';
import { VoucherController } from './voucher.controller';
import { UserModule } from '../users/user.module';

@Module({
    imports: [TypeOrmModule.forFeature([Voucher]), UserModule],
    providers: [VoucherService],
    exports: [TypeOrmModule, VoucherService],
    controllers: [VoucherController]
})
export class VoucherModule {}
