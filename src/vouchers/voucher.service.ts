import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Voucher } from './voucher.entity';
import { UserService } from '../users/user.service';

@Injectable()
export class VoucherService {
    constructor(
        @InjectRepository(Voucher)
        private vouchersRepository: Repository<Voucher>,
        private userService: UserService
    ) {}

    async getByUser(email: string): Promise<Voucher[]> {
        return await this.vouchersRepository.find({
            where: {
                user: {
                    email
                }
            }
        });
    }

    async get(id: number): Promise<Voucher> {
        return await this.vouchersRepository.findOne(id);
    }

    async getAll(): Promise<Voucher[]> {
        return await this.vouchersRepository.find();
    }

    async create(email: string, amount: number, details?: string): Promise<Voucher> {
        const voucher = new Voucher();
        voucher.amount = amount;
        voucher.remainingAmount = amount;
        voucher.details = details;
        const user = await this.userService.findOne(email);
        voucher.user = user;
        return await this.vouchersRepository.save(voucher);
    }

    async remove(voucher: Voucher): Promise<void> {
        await this.vouchersRepository.delete(voucher);
        return;
    }

    async setRemaining(voucher: Voucher, amount: number): Promise<Voucher> {
        voucher.remainingAmount = amount;
        return await this.vouchersRepository.save(voucher);
    }
}